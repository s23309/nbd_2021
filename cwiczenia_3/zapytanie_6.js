// 6. Dodaj siebie do bazy, zgodnie z formatem danych użytych dla innych osób (dane dotyczące karty kredytowej, adresu zamieszkania i wagi mogą być fikcyjne)
db.people.insert({
  sex: 'Male',
  first_name: 'Mateusz',
  last_name: 'Soczewka',
  job: 'Data Scientist',
  email: 's23309@pjwstk.edu.pl',
  location: {
    city: 'Marki',
    address: { streetname: 'Wspolna', streetnumber: '85' }
  },
  description: "Jakis opis blablabla",
  height: 185,
  weight: 69,
  birth_date: '1997-07-01T03:24:22Z',
  nationality: 'Poland',
  credit: [
    {
      type: 'mastercard',
      number: '4522537053556991',
      currency: 'PLN',
      balance: '9999.99'
    }
  ]
})
