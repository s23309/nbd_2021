// CWICZENIA 1


// 1.a
val dnitygodnia: List[String] = List("poniedzialek", "wtorek", "sroda", "czwartek", "piatek", "sobota", "niedziela")

var result : String = ""
for (n <- dnitygodnia) result = result + n + ','
println(result)

// 1.b
var result : String = ""
for (n <- dnitygodnia){
	if( n.substring(0,1).equals("p") ){
		result = result + n + ','
	}
}
println(result)

// 1.c
var result : String = ""
var a = dnitygodnia.length
while( a > 0 ){
    result = result + dnitygodnia(7-a) + ','
    a = a - 1;
}
println(result)


// ZADANIE 2
// 2.a
var indeks : Int = 0
var result : String = ""
var a = dnitygodnia.length

def append(str : String, indeks : Int) : (String, Int) = {
	if (indeks == 0){
		result = result + dnitygodnia(6-indeks)
		return (result, -1)
	}
	else{
		result = result + dnitygodnia(6-indeks) + ','
		return append(result, indeks-1)
	}
}

append(str = "", indeks = 6)._1


// 2.b
var indeks : Int = 0
var result : String = ""
var a = dnitygodnia.length

def append(str : String, indeks : Int) : (String, Int) = {
	if (indeks == 0){
		result = result + dnitygodnia(indeks)
		return (result, -1)
	}
	else{
		result = result + dnitygodnia(indeks) + ','
		return append(result, indeks-1)
	}
}

append(str = "", indeks = 6)._1


// 3
import scala.annotation.tailrec
@tailrec
def append(i: Int, str: String) : String = {
    if (i == dnitygodnia.length){
        return str
    }
    val currentDay = if (i == dnitygodnia.length - 1) dnitygodnia(i) else dnitygodnia(i) + ","
    append(i + 1, str + currentDay)
}

append(i = 0, "")



// 4.a 
dnitygodnia.fold("") {(akumulacja, nastepny) => {
    akumulacja + nastepny + ","
}}


// 4.b
dnitygodnia.foldRight("") {(akumulacja, nastepny) => {
    akumulacja + "," + nastepny
}}


// 4.c
dnitygodnia.fold("") {(akumulacja, nastepny) => {
    if (nastepny.startsWith("p")){
        akumulacja + nastepny + ","
    }
    else akumulacja
}}


// 5
val produkty = Map("buty" -> 250, "koszula" -> 100, "rekawiczki" -> 50, "spodnie" -> 100)
val poobnizce = produkty map {
    case (key, value) => (key, value * 0.9)
}
println(poobnizce)


// 6
def tuple_function(tuple: (Boolean, String, Int)) = {
    println(tuple._1)
    println(tuple._2)
    println(tuple._3)
}
tuple_function(true, "Jakis napis", 5)


// 7
val wez_buty = produkty.get("buty")
println(wez_buty)


// 8
val lista_z_zerami = List(7,0,0,5,0,1,0,1,2,5,7,0,0)

def usun_n(list: List[Int], n: Int): List[Int] = list match {
    case Nil => Nil
    case h :: t =>
        if (h == n)
            usun_n(t, n) // 0 found, skip it and iterate the tail
        else
            h :: usun_n(t, n)
}

usun_n(lista_z_zerami, 0)


// 9
def zwieksz_o_1(lista: List[Int]): List[Int] = {
    lista map (i => i + 1)
}
zwieksz_o_1(lista_z_zerami)


// 10
val jakas_lista = List(-50,-5,0,2,3,10,12,100)

def filtruj_abs(lista: List[Int]): List[Int] = {
    // przefiltrujemy najpier liste
    val lista_fil = lista filter (i => i <= 12 && i >= -5)
    return lista_fil map (i => i.abs)
}

filtruj_abs(jakas_lista)


