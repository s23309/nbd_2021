// 5. Średnia i łączna ilość środków na kartach kredytowych kobiet narodowości polskiej w podziale na waluty

printjson(
db.people.aggregate([
  {
    $match: { nationality: 'Poland', sex: 'Female' }
  },
  {
    $unwind: {
      path: '$credit'
    }
  },
  {
    $group: {
      _id: '$credit.currency',
      suma: {
        $sum: '$credit.balance'
      },
      srednia: {
        $avg: '$credit.balance'
      }
    }
  }
]).toArray()
)
