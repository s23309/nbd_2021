// 5. Średnia i łączna ilość środków na kartach kredytowych kobiet narodowości polskiej w podziale na waluty

printjson(
db.people.mapReduce(
  function () {
    this.credit.forEach(x => {
      emit(x.currency, x.balance);
    });
  },
  function (key, values) {
    const lacznie = Array.sum(values);
    const srednia = lacznie / values.length;

    return { lacznie, srednia };
  },
  {
    query: {
      nationality: 'Poland',
      sex: 'Female'
    },
    out: 'polish-balances'
  }
).find().toArray()
)
