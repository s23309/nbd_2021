// 4. Średnie, minimalne i maksymalne BMI (waga/wzrost^2) dla osób w bazie, w podziale na narodowości

printjson(
db.people.mapReduce(
  function () {
    let weight = parseFloat(this.weight)
    let height = parseFloat(this.height)
    emit(this.nationality, { weight:weight, height: height });
  },
  function (key, values) {
    let sumaBMI = 0;
    let maxBMI = 0;
    let minBMI = 999999;
    let count = 0;
    values.forEach(person => {
      const BMI = person.weight / ((person.height/100)*(person.height/100));
      sumaBMI += BMI;
      count += 1;

      if (BMI > maxBMI){
	maxBMI = BMI
      };
      if (BMI < minBMI){
	minBMI = BMI
      };
    });

    return {avgBMI: sumaBMI/count, maxBMI, minBMI};
  },
  { out: { inline: 1 } }
).find()
)
