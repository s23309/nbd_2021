#############################
##### MATEUSZ SOCZEWKA ######
########## s23309 ###########
#############################

import riak

# client
myClient = riak.RiakClient(pb_port=8087)

# Tworzymy bucketa
myBucket = myClient.bucket('chimichanga')

# Wrzucamy dokument "val1" do bazy z kluczem "klucz1"
val1 = {"imie": "Mateusz", "nazwisko": "Soczewka", "index": "s23309"}
key1 = myBucket.new('klucz1', data=val1)
key1.store()
print('Plik dodany do bazy')

# Pobieramy plik z bazy
fetched1 = myBucket.get('klucz1')
print('Plik fetched1 został pobrany.')

# Wypisujemy na ekran
print('Zawartość pliku:')
print(fetched1.data)

# Modyfikujemy dokument
fetched1.data["imie"] = 'Mati'
fetched1.store()
print('Dokument został zmodyfikowany.')

# Znowu pobieramy i wypisujemy
fetched1 = myBucket.get('klucz1')
print('Zawartość pliku po zmianie:')
print(fetched1.data)

# Usuwamy dokument z bazy
fetched1.delete()
print('Dokument usuniety.')

# Proba pobrania z bazy
fetched1 = myBucket.get('klucz1')
print('Proba pobrania usunietego pliku:')
print(fetched1.data)
