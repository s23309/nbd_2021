// CWICZENIA 2

// ZADANIE 1
// Wykorzystaj Pattern Matching w funkcji przyjmującej parametr typu String. Dla stringów odpowiadających nazwom dni tygodnia funkcja ma zwrócić „Praca” i „Weekend” (odpowiednio dla dni roboczych i wolnych), dla pozostałych napisów „Nie ma takiego dnia”.
def czy_weekend(str: String): String = {
    val robocze = List("poniedzialek", "wtorek", "sroda", "czwartek", "piatek")
    val weekend = List("sobota", "niedziela")
    str match {
      case x if (robocze.contains(x)) => "Praca"
      case y if (weekend.contains(y)) => "Weekend"
      case _ => "Nie ma takiego dnia"
    }
}

println(czy_weekend("poniedzialek"))
println(czy_weekend("sobota"))
println(czy_weekend("inny napis"))



// ZADANIE 2
// Zdefiniuj klasę KontoBankowe z metodami wplata i wyplata oraz własnością stanKonta - własność ma być tylko do odczytu. Klasa powinna udostępniać konstruktor przyjmujący początkowy stan konta oraz drugi, ustawiający początkowy stan konta na 0.
class KontoBankowe(pocz_stan_konta: Double) {
    private var stanKonta_pv = pocz_stan_konta
    def stanKonta: Double = stanKonta_pv

    def this(){
        this(0)
    }
    
    def wplata(x: Double): Double = {
        stanKonta_pv = stanKonta_pv + x
        stanKonta_pv
    }
    
    def wyplata(x: Double): Double = {
        if (stanKonta_pv - x < 0){
            return stanKonta_pv
        } else{
            stanKonta_pv = stanKonta_pv - x
            return stanKonta_pv
        }
    }
}

val konto_jacka = new KontoBankowe(10000)
println(konto_jacka.stanKonta)

val konto_ewy = new KontoBankowe()
println(konto_ewy.stanKonta)
println(konto_ewy.wplata(2500))
println(konto_ewy.wyplata(500))



// ZADANIE 3
// Zdefiniuj klasę Osoba z własnościami imie i nazwisko. Stwórz kilka instancji tej klasy. Zdefiniuj funkcję, która przyjmuje obiekt klasy osoba i przy pomocy Pattern Matching wybiera i zwraca napis zawierający przywitanie danej osoby. Zdefiniuj 2-3 różne przywitania dla konkretnych osób (z określonym imionami lub nazwiskami) oraz jedno domyślne. 
case class Osoba(val imie: String, val nazwisko: String)

val osoba_1 = Osoba("Jan", "Nowak")
val osoba_2 = Osoba("Adam", "Kowalski")
val osoba_3 = Osoba("Ewa", "Nowakowska")

def greeting(osoba: Osoba): String = {
    val przywitanie = osoba match {
        case Osoba("Jan", "Nowak") => "Siemka! Jestem Janek."
        case Osoba("Adam", "Kowalski") => "Witaj, nazywam sie Piotrek."
        case _ => "Czesc, pozdrawiam"
    }
    return przywitanie
}

greeting(osoba_1)
greeting(osoba_2)
greeting(osoba_3)




// ZADANIE 4
// Zdefiniuj funkcję przyjmującą dwa parametry - wartość całkowitą i funkcję operującą na wartości całkowitej. Zastosuj przekazaną jako parametr funkcję trzykrotnie do wartości całkowitej i zwróć wynik.

val intiger = 1
def skaler(x: Int): Int = x*3
def map_skaler(number: Int, fun: (Int) => Int): Int = {
    return fun(fun(fun(number)))
}

println(map_skaler(number = intiger, fun = skaler))




// ZADANIE 5
// Zdefiniuj klasę Osoba i trzy traity: Student, Nauczyciel, Pracownik. Osoba powinna mieć własności read only: imie, nazwisko, podatek. Pracownik powinien mieć własność pensja (z getterem i seterem). Student i Pracownik powinni przesłaniać własność podatek – dla Studenta zwracamy 0, dla Pracownika 20% pensji. Nauczyciel powinien dziedziczyć z Pracownika, dla niego podatek zwraca 10% pensji. Stwórz obiekty z każdym z traitów, pokaż jak podatek działa dla każdego z nich. Stwórz obiekty z traitami Student i Pracownik, pokaż jak podatek zadziała w zależności od kolejności w jakiej te traity zostały dodane przy tworzeniu obiektu. 


class Osoba(val imie: String, val nazwisko: String) {
  val podatek = 0
}

trait Student extends Osoba {
  override val podatek: Int = 0
}

trait Pracownik extends Osoba {
  override val podatek: Int = 20
  var pensja: Double = 0
}

trait Nauczyciel extends Pracownik {
  override val podatek: Int = 10
}



val jan_student = new Osoba("Jan", "Nowak") with Student
println(s"Student zapłaci podatek w wyosokści: ${jan_student.podatek}%")

val adam_pracownik = new Osoba("Adam", "Kowalski") with Pracownik
println(s"Pracownik zapłaci podatek w wyosokści ${adam_pracownik.podatek}%")

val ewa_nauczyciel = new Osoba("Ewa", "Nowakowska") with Nauczyciel
println(s"Nauczyciel zapłaci podatek w wysokości ${ewa_nauczyciel.podatek}%")

val kuba_pracownik_student = new Osoba("Kuba", "Guzik") with Pracownik with Student
println(s"Kuba najpierw pracownik a potem student zapłaci podatek w wysokości ${kuba_pracownik_student.podatek}%")

val kuba_student_pracownik = new Osoba("Kuba", "Guzik") with Student with Pracownik
println(s"Kuba najpierw student a potem pracownik zapłaci podatek w wysokościi ${kuba_student_pracownik.podatek}%")













